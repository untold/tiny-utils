# About Tiny Utils

Use ItsTheConnection's Tiny Utils to extend some functionalities for the Unity Editor.

# Installing Tiny Utils

To install this package, follow the instructions in the [Package Manager documentation](https://docs.unity3d.com/Packages/com.unity.package-manager-ui@latest/index.html).

# Using Tiny Utils

The Tiny Utils Manual can be found [here] (https://docs.unity3d.com/Manual/SpriteEditor.html).


# Technical details
## Requirements

This version of Tiny Utils is compatible with the following versions of the Unity Editor:

* 2019.4 and later (recommended)

## Package contents

The following table indicates the folder structure of the Sprite package:

|Location|Description|
|---|---|
|`<Editor>`|Root folder containing the source for the Tiny Utils.|
|`<Runtime>`|Root folder containing the source for the Tiny Utils's runtime usable methods.|

## Document revision history

|Date|Reason|
|---|---|
|Jenuary 7, 2021|Document created. Matches package version 0.1.0|
