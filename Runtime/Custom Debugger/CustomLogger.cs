﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace TinyUtils.CustomDebugger
{
    public static class CustomLogger
    {
        #region Simple Logs
        /// <summary>
        /// Logs a message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        /// <param name="type">The type of the message.</param>
        public static void Log(string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = stackTrace.GetFrame(1).GetType().GetHashCode() + line;

            var logInfo = new LogInfo(null, title, content, MessageType.None, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogInfo(string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = stackTrace.GetFrame(1).GetType().GetHashCode() + line;

            var logInfo = new LogInfo(null, title, content, MessageType.Info, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a warning styled message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogWarning(string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = stackTrace.GetFrame(1).GetType().GetHashCode() + line;

            var logInfo = new LogInfo(null, title, content, MessageType.Warning, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a error styled message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogError(string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = stackTrace.GetFrame(1).GetType().GetHashCode() + line;

            var logInfo = new LogInfo(null, title, content, MessageType.Error, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }
        #endregion

        #region Simple Logs Timed
        /// <summary>
        /// Logs a message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// <para>Keeping Time into account.</para>
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        /// <param name="type">The type of the message.</param>
        public static void LogT(string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = (int)(Time.time * 100);

            var logInfo = new LogInfo(null, title, content, MessageType.None, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// <para>Keeping Time into account.</para>
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogInfoT(string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = (int)(Time.time * 100);

            var logInfo = new LogInfo(null, title, content, MessageType.Info, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a warning styled message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// <para>Keeping Time into account.</para>
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogWarningT(string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = (int)(Time.time * 100);

            var logInfo = new LogInfo(null, title, content, MessageType.Warning, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a error styled message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// <para>Keeping Time into account.</para>
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogErrorT(string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = (int)(Time.time * 100);

            var logInfo = new LogInfo(null, title, content, MessageType.Error, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }
        #endregion

        #region Object Reference
        /// <summary>
        /// Logs a message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void Log(this Object caller, string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = stackTrace.GetFrame(1).GetType().GetHashCode() + line;

            var logInfo = new LogInfo(caller, title, content, MessageType.None, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogInfo(this Object caller, string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = stackTrace.GetFrame(1).GetType().GetHashCode() + line;

            var logInfo = new LogInfo(caller, title, content, MessageType.Info, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a warning styled message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogWarning(this Object caller, string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = stackTrace.GetFrame(1).GetType().GetHashCode() + line;

            var logInfo = new LogInfo(caller, title, content, MessageType.Warning, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a error styled message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogError(this Object caller, string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = stackTrace.GetFrame(1).GetType().GetHashCode() + line;

            var logInfo = new LogInfo(caller, title, content, MessageType.Error, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }
        #endregion

        #region Object Reference Timed
        /// <summary>
        /// Logs a message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// <para>Keeping Time into account.</para>
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogT(this Object caller, string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = (int)(Time.time * 1000);

            var logInfo = new LogInfo(caller, title, content, MessageType.None, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// <para>Keeping Time into account.</para>
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogInfoT(this Object caller, string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = (int)(Time.time * 1000);

            var logInfo = new LogInfo(caller, title, content, MessageType.Info, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a warning styled message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// <para>Keeping Time into account.</para>
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogWarningT(this Object caller, string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = (int)(Time.time * 1000);

            var logInfo = new LogInfo(caller, title, content, MessageType.Warning, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }

        /// <summary>
        /// Logs a error styled message to the <see cref="CustomDebuggerEW"/> Editr Window.
        /// <para>Keeping Time into account.</para>
        /// </summary>
        /// <param name="title">The title to display.</param>
        /// <param name="content">A more detailed content describing the message.</param>
        public static void LogErrorT(this Object caller, string title, string content = null) {
#if UNITY_EDITOR
            if (CustomDebuggerEW.Instance == null)
                return;

            var stackTrace = new System.Diagnostics.StackTrace(true);
            var fileName = stackTrace.GetFrame(1).GetFileName();
            var line = stackTrace.GetFrame(1).GetFileLineNumber();

            int id = (int)(Time.time * 1000);

            var logInfo = new LogInfo(caller, title, content, MessageType.Error, id, fileName, line, 1);

            CustomDebuggerEW.Instance.Log(logInfo);
#endif
        }
        #endregion
    }
}