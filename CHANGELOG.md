# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [0.1.0] - 2021-01-7
### This is the first release of Tiny Utils, as a Package
### It includes a basic Batch Rename feature, useful for renaming multiple objects with fewer clicks.

## [0.2.0] - 2021-04-12
### Update with new feature for custom debugging.
### Added custom debugger window for managing a separate debug system.
### Gitattributes was added and files were moved to LFS.

## [0.3.0] - 2021-04-13
### Update with new feature for custom debugging.
### Debugs are simpler to use.
### Debugs can now open the code or ping the calling object.

## [0.3.5] - 2021-04-13
### Update with new feature for custom debugging.
### Toolbar preferences are now saved to EditorPrefs.
### Added Link Button to the list element which allows to open the script quickly.

## [0.4.0] - 2021-04-13
### Update with new feature for in-game console.
### It toggles by pressing the "Backslash(\)" key.
### Allows for creating a commandline console for in-game purpose.
### By default it supports "help" and "clear" commands.

## [0.4.5] - 2021-04-13
### Update with new feature for custom debugging.
### Improved Logging feature, now it's simpler to log to the Custom Console.
### Improved Window UI.

## [0.4.8] - 2021-04-27
### Update with new feature for custom debugging.
### Added new category for logging in filters.
### Improved Logging feature, now logger supports separating logs based on the time they were called.
### Fixed Window UI.

## [0.5.0] - 2021-05-03
### Update with new feature for Inheriter.
### Fixed inheriting feature.
### Added creation of Custom Inspector for selected script.

## [1.0.0] - 2021-07-17
### Values set by user are now saved in EditorPrefs
### Rename toggle now has rename icon
### Fixed bug where renaming scene objects didn't set scene dirty

## [1.1.0] - 2021-07-22
### Added Undo recording for rename and replace actions
### Fixed bug which allowed renaming assets but didn't really work

## [1.2.0] - 2021-10-19
### Made project buildable when using CustomDebugger