using System.IO;
using System.Collections.Generic;
using UnityEditor;
using System;
using System.Reflection;
using System.Text;
using System.Linq;

namespace TinyUtils.Inheriter
{
    public class Inheriter : Editor
    {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Editor Callbacks
        [MenuItem("Assets/Derive From Selected", priority = 5000)]
        public static void DeriveFromSelectedClass () {
            WriteDerivedClass();
        }

        [MenuItem("Assets/Derive From Selected", true)]
        public static bool DeriveFromSelectedClassValidator () {
            return Selection.activeObject != null && Selection.activeObject is MonoScript;
        }

        [MenuItem("Assets/Custom Inspect/To Local Editor Folder", priority = 5001)]
        public static void CustomInspectSelectedClass () {
            WriteCustomInspectForClass(true);
        }
        
        [MenuItem("Assets/Custom Inspect/Here", priority = 5001)]
        public static void CustomInspectSelectedClassAlt () {
            WriteCustomInspectForClass(false);
        }

        [MenuItem("Assets/Custom Inspect/To Local Editor Folder", true)]
        [MenuItem("Assets/Custom Inspect/Here", true)]
        public static bool CustomInspectSelectedClassValidator () {
            return false;
            // return Selection.activeObject != null && Selection.activeObject is MonoScript && (Selection.activeObject as MonoScript).GetClass().IsSubclassOf(typeof(UnityEngine.Object));
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Calls the <see cref="DeriveClassWithOverridables(MonoScript, Type)"/> and writes its result to file in the Assets.
        /// </summary>
        private static void WriteDerivedClass () {
            var script = (MonoScript)Selection.activeObject;
            var path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(Selection.activeObject)) + $"/{script.GetClass().Name}Inherit.cs";
            var uniquePath = AssetDatabase.GenerateUniqueAssetPath(path);
            File.WriteAllText(uniquePath, DeriveClassWithOverridables(script, script.GetClass(), Path.GetFileNameWithoutExtension(uniquePath)));
            AssetDatabase.ImportAsset(uniquePath);
        }

        /// <summary>
        /// Calls the <see cref="MakeCustomInspectorForClass(Type)"/> and writes its result to file in the Assets/*/Editor.
        /// </summary>
        private static void WriteCustomInspectForClass (bool createEditorFolder) {
            var script = (MonoScript)Selection.activeObject;
            var dirPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(Selection.activeObject));

            if (createEditorFolder)
                dirPath += "/Editor/";

            if(!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            var path = dirPath + $"/{script.GetClass().Name}CE.cs";
            File.WriteAllText(path, MakeCustomInspectorForClass(script.GetClass()));
            AssetDatabase.ImportAsset(path);
        }

        /// <summary>
        /// Given a <see cref="MonoScript"/> and and a <see cref="Type"/>, constructs a new class which derives from the given one.
        /// </summary>
        /// <param name="script">The origin class to derive from.</param>
        /// <param name="t">The type of the origin class.</param>
        /// <returns>The <see cref="string"/> containing the newly created class.</returns>
        private static string DeriveClassWithOverridables (MonoScript script, Type t, string uniqueClassName) {
            StringBuilder classAsText = new StringBuilder();

            IEnumerable<string> lines = File.ReadLines(AssetDatabase.GetAssetPath(Selection.activeObject));
            foreach(var line in lines) {
                if(line.StartsWith("using"))
                    classAsText.Append(line + "\n");
                else
                    break;
            }

            /// Adding Namespace
            classAsText.Append("\nnamespace " + (!string.IsNullOrEmpty(t.Namespace) ? t.Namespace : "GeneratedNamespace") + "\n{{");

            /// Defining Class
            classAsText.Append("\n    public class {0} : {1}\n    {{");

            /// Region - Defining Variables
            classAsText.Append("\n        #region Public Variables");
            classAsText.Append("\n        #endregion");
            classAsText.Append("\n\n        #region Private Variables");
            classAsText.Append("\n        #endregion");

            /// Region - Defining Overrides
            classAsText.Append("\n\n        #region Overrides");

            /// Cicles all methods and picks the overridable ones
            foreach(var prop in t.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly).Where(p => p.IsAbstract || p.IsVirtual)) {
                /// Starts method's declaration
                classAsText.Append("\n        " + GetVisibility(prop) + " override ");

                /// Defines method's return type
                try {
                    classAsText.Append(Aliases[prop.ReturnType]); // Base Ones
                } catch {
                    classAsText.Append(prop.ReturnType.Name); // Custom Ones
                }

                /// Defines method's name
                classAsText.Append(" " + prop.Name + "(");

                /// Starts the method's parameters definition
                if(prop.GetParameters().Length > 0) {
                    foreach(var pars in prop.GetParameters()) {
                        /// Defines parameter's type
                        try {
                            classAsText.Append(Aliases[pars.ParameterType]); // Base Ones
                        } catch {
                            classAsText.Append(pars.ParameterType.Name); // Custom Ones
                        }

                        /// Defines parameter's name
                        classAsText.Append(" " + pars.Name + ", ");
                    }

                    /// Removes the last 2 characters ", "
                    classAsText.Remove(classAsText.Length - 2, 2);
                }

                /// Closes the method's parameters definition and method's declaration
                classAsText.Append(/*" " + prop.Name + "(*/") {{\n"
                    + (prop.ReturnType != typeof(void) ? "            throw new System.NotImplementedException();" : "")
                    + "\n        }}\n");
            }

            classAsText.Append("        #endregion");

            /// Region - Defining Methods
            classAsText.Append("\n\n        #region Private Methods");
            classAsText.Append("\n        #endregion");
            classAsText.Append("\n\n        #region Public Methods");
            classAsText.Append("\n        #endregion");

            classAsText.Append("\n    }}");
            classAsText.Append("\n}}");

            return string.Format(classAsText.ToString(), uniqueClassName, t.Name);
        }

        /// <summary>
        /// Given a <see cref="MonoScript"/> and and a <see cref="Type"/>, constructs a new templated custom inspector for it.
        /// <para><b>[Warning]</b> Overwrites existing one with same name if it exists.</para>
        /// </summary>
        /// <param name="t">The type of the origin class.</param>
        /// <returns>The <see cref="string"/> containing the newly created class.</returns>
        private static string MakeCustomInspectorForClass (Type t) {
            StringBuilder classAsText = new StringBuilder();

            /// Define Usings
            classAsText.AppendLine("using System.Collections;");
            classAsText.AppendLine("using System.Collections.Generic;");
            classAsText.AppendLine("using UnityEngine;");
            classAsText.AppendLine("using UnityEditor;");
            
            /// Add source namespace if exists
            if(!string.IsNullOrEmpty(t.Namespace))
                classAsText.AppendLine($"using {t.Namespace};");

            /// Adding Namespace
            classAsText.Append("\nnamespace " + (!string.IsNullOrEmpty(t.Namespace) ? t.Namespace + ".CustomInspectors" : "CustomInspectors") + "\n{{");

            /// Defining Attribute
            classAsText.Append($"\n    [CustomEditor(typeof({t.Name}))]");

            /// Defining Class
            classAsText.Append("\n    public class {0}CE : Editor\n    {{");

            /// Region - Defining Variables
            classAsText.Append("\n        #region Public Variables");
            classAsText.Append("\n        #endregion");
            classAsText.Append("\n\n        #region Private Variables");
            classAsText.Append("\n        #endregion");

            /// Region - Defining Overrides
            classAsText.Append("\n\n        #region GUI");

            classAsText.AppendLine("\n        public override void OnInspectorGUI () {{");
            classAsText.AppendLine("            serializedObject.Update();");

            classAsText.AppendLine("\n            #region GUI Here");
            classAsText.AppendLine("            #endregion");

            classAsText.AppendLine("\n            serializedObject.ApplyModifiedProperties();");
            classAsText.AppendLine("        }}");

            classAsText.Append("        #endregion");

            /// Region - Defining Methods
            classAsText.Append("\n\n        #region Private Methods");
            classAsText.Append("\n        #endregion");
            classAsText.Append("\n\n        #region Public Methods");
            classAsText.Append("\n        #endregion");

            classAsText.Append("\n    }}");
            classAsText.Append("\n}}");

            return string.Format(classAsText.ToString(), t.Name);
        }

        #region C# Sadness
        /// <summary>
        /// Given a <see cref="MethodInfo"/> determines its visibility.
        /// </summary>
        /// <param name="m">The <see cref="MethodInfo"/> to check.</param>
        /// <returns>A <see cref="string"/> between <b>public, private, protected, internal</b></returns>
        static string GetVisibility (MethodInfo m) {
            string visibility = "";
            if(m.IsPublic)
                return "public";
            else if(m.IsPrivate)
                return "private";
            else if(m.IsFamily)
                visibility = "protected";
            else if(m.IsAssembly)
                visibility += "internal";
            return visibility;
        }

        /// <summary>
        /// Given a base <see cref="Type"/> (eg: int, float, string ...) returns the <see cref="string"/> with the corresponding name.
        /// </summary>
        private static readonly Dictionary<Type, string> Aliases = new Dictionary<Type, string>()
        {
            { typeof(byte), "byte" },
            { typeof(sbyte), "sbyte" },
            { typeof(short), "short" },
            { typeof(ushort), "ushort" },
            { typeof(int), "int" },
            { typeof(uint), "uint" },
            { typeof(long), "long" },
            { typeof(ulong), "ulong" },
            { typeof(float), "float" },
            { typeof(double), "double" },
            { typeof(decimal), "decimal" },
            { typeof(object), "object" },
            { typeof(bool), "bool" },
            { typeof(char), "char" },
            { typeof(string), "string" },
            { typeof(void), "void" }
        };
        #endregion

        #endregion

        #region Public Functions
        #endregion
    }
}