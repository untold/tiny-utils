using UnityEditor;
using UnityEngine;

namespace TinyUtils
{
    public static class EditroMeasures
    {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Getters
        public static float ToolbarHeight => EditorStyles.toolbar.fixedHeight;
        public static float FieldHeight => 21;
        public static float LowHeight => 19;
        #endregion

        #region Private Methods
        #endregion

        #region Public Functions
        #endregion
    }

    public static class EditorColors
    {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Getters
        public static Color Base => Color.white;
        public static Color NormalDark => new Color(0.23f, 0.23f, 0.23f, 1);
        public static Color ActiveLight => new Color(0.29f, 0.29f, 0.29f, 1);
        public static Color ActiveBlue => new Color(.17f, 0.36f, 0.53f, 1);
        #endregion

        #region Private Methods
        #endregion

        #region Public Functions
        #endregion
    }
}