using UnityEngine;
using UnityEditor;

namespace TinyUtils.BatchRename
{
    public class BatchRename : EditorWindow
    {
        #region Public Variables
        public GUISkin m_skin;
        #endregion

        #region Private Variables
        private string nameBase = "BaseName";
        private string nameToReplace = "...";
        private int startingFromNumber = 0;
        private enum DifferBy
        {
            DoNotDifferentiate,
            Number,
            Letter
        }
        private DifferBy differBy;
        private bool replacing = false;

        private const string PPK_Name = "TinyUtils-Name";
        private const string PPK_NameToReplace = "TinyUtils-NameToReplace";
        private const string PPK_Differ = "TinyUtils-Differ";
        private const string PPK_Replacing = "TinyUtils-Replacing";
        #endregion

        #region EditorWindow Callbacks
        [MenuItem("Window/Tiny Utils/BatchRename %F2")]
        public static void ShowWindow () {
            var win = GetWindow<BatchRename>();
            // var win = CreateInstance<BatchRename>();
            win.titleContent = new GUIContent("Batch Rename");
            // win.ShowModal();
        }

        private void OnEnable () {
            nameBase = EditorPrefs.GetString(PPK_Name, null);
            nameToReplace = EditorPrefs.GetString(PPK_NameToReplace, null);
            differBy = (DifferBy)EditorPrefs.GetInt(PPK_Differ, 0);
            replacing = EditorPrefs.GetBool(PPK_Replacing, false);

            maxSize = minSize = new Vector2(400, replacing ? 65 : (differBy == DifferBy.Number ? 85 : 65));
        }

        private void OnDisable() {
            EditorPrefs.SetString(PPK_Name, nameBase);
            EditorPrefs.SetString(PPK_NameToReplace, nameToReplace);
            EditorPrefs.SetInt(PPK_Differ, (int)differBy);
            EditorPrefs.SetBool(PPK_Replacing, replacing);
        }

        private void OnGUI () {
            EditorGUILayout.BeginHorizontal();
            {
                nameBase = EditorGUILayout.TextField(new GUIContent(replacing ? "Replace With" : "Base Name", "The name shared by all renamed objects\nUse $ to insert the differentiator."), nameBase);

                EditorGUI.BeginChangeCheck();
                replacing = GUILayout.Toggle(replacing, new GUIContent(string.Empty, "Toggles Replace Characters"), m_skin.GetStyle("Rename"), GUILayout.Width(20));
                if (EditorGUI.EndChangeCheck()) {
                    position = new Rect(position) {
                        height = replacing ? 55 : 75
                    };
                    maxSize = minSize = position.size;
                }
            }
            EditorGUILayout.EndHorizontal();

            if(replacing)
                nameToReplace = EditorGUILayout.TextField(new GUIContent("Original", "The name that you want to replace"), nameToReplace);
            else {
                EditorGUI.BeginChangeCheck();
                differBy = (DifferBy)EditorGUILayout.EnumPopup(new GUIContent("Differ By"), differBy);
                if (EditorGUI.EndChangeCheck()) {
                    position = new Rect(position) {
                        height = differBy == DifferBy.Number ? 75 : 55
                    };
                    maxSize = minSize = position.size;
                }
            }

            if (!replacing) {
                switch (differBy) {
                    case DifferBy.DoNotDifferentiate:
                        break;
                    case DifferBy.Number:
                        startingFromNumber = EditorGUILayout.IntField("Starting From", startingFromNumber);
                        break;
                    case DifferBy.Letter:
                        break;
                }
            }

            GUI.enabled = Selection.objects.Length > 0;
            EditorGUILayout.BeginHorizontal();
            {
                if(GUILayout.Button("Rename", EditorStyles.miniButtonLeft)) {
                    if (Selection.transforms != null && Selection.transforms.Length > 0)
                        RenameSelectedObjects();
                    else
                        RenameSelectedAssets();
                }
                if(GUILayout.Button("Rename And Close", EditorStyles.miniButtonRight)) {
                    if (Selection.transforms != null && Selection.transforms.Length > 0)
                        RenameSelectedObjects();
                    else
                        RenameSelectedAssets();
                    Close();
                }
            }
            EditorGUILayout.EndHorizontal();
            GUI.enabled = true;
        }
        #endregion

        #region Private Methods
        private void RenameSelectedObjects () {
            if(replacing) {
                Undo.RecordObjects(Selection.objects, "Batch Replace");
                for(int i = 0; i < Selection.objects.Length; i++) {
                    Selection.objects[i].name = Selection.objects[i].name.Replace(nameToReplace, nameBase);
                }
                return;
            }

            switch (differBy) {
                case DifferBy.DoNotDifferentiate:
                    Undo.RecordObjects(Selection.objects, "Batch Rename");
                    for (int i = 0; i < Selection.objects.Length; i++) {
                        Selection.objects[i].name = nameBase;
                    }
                    break;
                case DifferBy.Number:
                    Undo.RecordObjects(Selection.objects, "Batch Rename (Numbers)");
                    var startNumber = startingFromNumber;
                    for (int i = 0; i < Selection.objects.Length; i++) {
                        if(nameBase.Contains("$"))
                            Selection.objects[i].name = nameBase.Replace("$", $"{startNumber}");
                        else
                            Selection.objects[i].name = nameBase + startNumber;
                        ++startNumber;
                    }
                    break;
                case DifferBy.Letter:
                    Undo.RecordObjects(Selection.objects, "Batch Rename (Letters)");
                    for (int i = 0; i < Selection.objects.Length; i++) {
                        Selection.objects[i].name = nameBase + LetterCounter(i);
                    }
                    break;
                default:
                    break;
            }

            // if (!AssetDatabase.Contains(Selection.objects[0]))
            //     EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetSceneAt(0));
        }

        private void RenameSelectedAssets () {
            if(replacing) {
                Undo.RecordObjects(Selection.objects, "Assets Batch Replace");
                for(int i = 0; i < Selection.objects.Length; i++) {
                    AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(Selection.objects[i]), Selection.objects[i].name.Replace(nameToReplace, nameBase));
                    // Selection.objects[i].name = Selection.objects[i].name.Replace(nameToReplace, nameBase);
                }
                return;
            }

            switch (differBy) {
                case DifferBy.DoNotDifferentiate:
                    Undo.RecordObjects(Selection.objects, "Assets Batch Rename");
                    for (int i = 0; i < Selection.objects.Length; i++) {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(Selection.objects[i]), nameBase);
                        // Selection.objects[i].name = nameBase;
                    }
                    break;
                case DifferBy.Number:
                    Undo.RecordObjects(Selection.objects, "Assets Batch Rename (Numbers)");
                    for (int i = 0; i < Selection.objects.Length; i++) {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(Selection.objects[i]), nameBase + i);
                        // Selection.objects[i].name = nameBase + i;
                    }
                    break;
                case DifferBy.Letter:
                    Undo.RecordObjects(Selection.objects, "Assets Batch Rename (Letters)");
                    for (int i = 0; i < Selection.objects.Length; i++) {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(Selection.objects[i]), nameBase + LetterCounter(i));
                        // Selection.objects[i].name = nameBase + LetterCounter(i);
                    }
                    break;
                default:
                    break;
            }

            // if (!AssetDatabase.Contains(Selection.objects[0]))
            //     EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetSceneAt(0));
        }

        private string LetterCounter (int pos) {
            string id = "";
            int times = (pos / 26);

            for (int i = 0; i <= times; i++) {
                char c = (char)(65 + pos % (26 - (times - i) * 25));
                id += c;
                //pos = Mathf.Max(pos - 25, 0);
            }

            return id;
        }
        #endregion

        #region Public Functions
        #endregion
    }
}