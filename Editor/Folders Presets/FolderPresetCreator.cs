using System.IO;
using UnityEditor;

namespace TinyUtils.FoldersPresets {
    public static class FolderPresetCreator {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Public Functions
        [MenuItem("Assets/Create/Folder Preset/3D Model", priority = -100000)]
        public static void PrepareModelImportSetup() {
            var ctx = Selection.activeObject;
            var pathToCtx = AssetDatabase.GetAssetPath(ctx);

            var materialsFolderPath = Path.Combine(pathToCtx, "Materials");
            Directory.CreateDirectory(materialsFolderPath);

            var modelsFolderPath = Path.Combine(pathToCtx, "Models");
            Directory.CreateDirectory(modelsFolderPath);

            var prefabsFolderPath = Path.Combine(pathToCtx, "Prefabs");
            Directory.CreateDirectory(prefabsFolderPath);

            var texturesFolderPath = Path.Combine(pathToCtx, "Textures");
            Directory.CreateDirectory(texturesFolderPath);

            AssetDatabase.SaveAssets();
            AssetDatabase.ImportAsset(pathToCtx);
            AssetDatabase.Refresh();
        }
        #endregion

        #region Private Methods
        #endregion
    }
}