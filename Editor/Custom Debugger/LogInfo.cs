﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace TinyUtils.CustomDebugger
{
    [System.Serializable]
    public class Logs
    {
        public List<LogInfo> logs = new List<LogInfo>();
    }

    [System.Serializable]
    public class LogInfo
    {
        public int callerInstanceID;
        public string file;
        public int line;
        public string title;
        public string content;
        public MessageType type;
        public int counter = 1;
        public int key;

        public LogInfo (LogInfo log) {
            this.callerInstanceID = log.callerInstanceID;
            this.file = log.file;
            this.title = log.title;
            this.content = log.content;
            this.type = log.type;
            this.counter = log.counter;
            this.key = log.key;
        }

        public LogInfo (Object caller, string title, string content, MessageType type = MessageType.Info) {
            this.callerInstanceID = caller != null ? caller.GetInstanceID() : 0;
            this.title = title;
            this.content = content;
            this.type = type;
        }

        public LogInfo (Object caller, string title, string content, MessageType type, int id) {
            this.callerInstanceID = caller != null ? caller.GetInstanceID() : 0;
            this.title = title;
            this.content = content;
            this.type = type;
            this.key = id;
        }

        public LogInfo (Object caller, string title, string content, MessageType type, int id, string fileName, int counter = 1) {
            this.callerInstanceID = caller != null ? caller.GetInstanceID() : 0;
            this.title = title;
            this.content = content;
            this.type = type;
            this.key = id;
            this.file = fileName;
            this.line = 0;
            this.counter = counter;
        }

        public LogInfo (Object caller, string title, string content, MessageType type, int id, string fileName, int line, int counter = 1) {
            this.callerInstanceID = caller != null ? caller.GetInstanceID() : 0;
            this.title = title;
            this.content = content;
            this.type = type;
            this.key = id;
            this.file = fileName;
            this.line = line;
            this.counter = counter;
        }
    }
}