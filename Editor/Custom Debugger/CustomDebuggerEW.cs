using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;

namespace TinyUtils.CustomDebugger
{
    public class CustomDebuggerEW : EditorWindow
    {
        #region Public Variables
        public GUISkin _skin;

        public static CustomDebuggerEW Instance;
        #endregion

        #region Private Variables
        private Dictionary<int, LogInfo> _logs = new Dictionary<int, LogInfo>();
        private string[] _objNames = new string[1] { "All" };
        private int[] _objKeys = new int[1] { -1 };
        private int[] _logsTypesCount = new int[4] { 0, 0, 0, 0 };

        private Vector2 _scroll;
        private Rect _lastRect;
        private Rect _resizeRect;
        private bool _resizing;
        private float _panelHeight;

        private int _selectedLogID = 0;
        private int _filterObjectID = -1;
        private bool[] _filterMsgType;

        private bool _clearOnAwake = true;

        private string _logsFile = "";
        #endregion

        #region Constructor
        [MenuItem("Window/Tiny Utils/Debugger %&D")]
        public static void ShowWindow () {
            var win = GetWindow<CustomDebuggerEW>();
            win.titleContent = new GUIContent("Custom Logger", EditorGUIUtility.IconContent("UnityEditor.ConsoleWindow").image);
            win.wantsMouseMove = true;
            win.Show();
        }
        #endregion

        #region EditorWindow Callbacks

        #region PlayMode Change
        private void OnEnable () {
            Instance = this;

            _filterMsgType = new bool[4] { EditorPrefs.GetBool("CusDeb_FilterInfo", true), EditorPrefs.GetBool("CusDeb_FilterWarn", true), EditorPrefs.GetBool("CusDeb_FilterError", true), EditorPrefs.GetBool("CusDeb_FilterNone", true) };

            _clearOnAwake = EditorPrefs.GetBool("CusDeb_ClearOnAwake", true);

            _panelHeight = position.height * 0.7f;

            _logsFile = Application.dataPath.Replace("/Assets", "/Logs/CustomLogs.cs");

            if(_clearOnAwake)
                ClearLogs();
            else
                LoadLogs();
        }

        void OnDisable () {
            SaveLogs();

            EditorPrefs.SetBool("CusDeb_FilterInfo", _filterMsgType[0]);
            EditorPrefs.SetBool("CusDeb_FilterWarn", _filterMsgType[1]);
            EditorPrefs.SetBool("CusDeb_FilterError", _filterMsgType[2]);
            EditorPrefs.SetBool("CusDeb_FilterNone", _filterMsgType[3]);

            EditorPrefs.SetBool("CusDeb_ClearOnAwake", _clearOnAwake);
        }
        #endregion

        private void OnGUI () {
            wantsMouseMove = true;

            Event e = Event.current;

            DrawToolbar(e);

            DrawUpperPanel(e);

            #region Resizing
            _resizeRect = new Rect(0, _panelHeight - 4 + EditroMeasures.ToolbarHeight, position.width, 8);
            EditorGUIUtility.AddCursorRect(_resizeRect, MouseCursor.ResizeVertical);

            Handles.color = Color.black * 0.4f;
            Handles.DrawLine(new Vector2(0, _panelHeight + EditroMeasures.ToolbarHeight), new Vector2(position.width, _panelHeight + EditroMeasures.ToolbarHeight));
            Handles.color = Color.white;

            if(e.type == EventType.MouseDown) {
                if(_resizeRect.Contains(e.mousePosition)) {
                    _resizing = true;
                }
            } else if (e.type == EventType.MouseUp) {
                _resizing = false;
            }

            if(_resizing) {
                _panelHeight = e.mousePosition.y - EditroMeasures.ToolbarHeight;
            }
            _panelHeight = Mathf.Clamp(_panelHeight, position.height * 0.4f, position.height * 0.8f);
            #endregion

            DrawLowerPanel(e);

            if(EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying) { // This is actually called twice
                SaveLogs();
            }

            if(e.isKey || e.isMouse || e.type == EventType.MouseMove)
                Repaint();
        }
        #endregion

        #region GUI

        #region Toolbar
        private void DrawToolbar (Event e) {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                #region Clearing
                var dropdownClear = EditorGUILayout.DropdownButton(new GUIContent("Clear"), FocusType.Passive, EditorStyles.toolbarDropDown, GUILayout.Width(54));
                _lastRect = new Rect(Vector2.zero, GUILayoutUtility.GetLastRect().size - Vector2.right * 16);

                if(dropdownClear) {
                    if(e.mousePosition.x >= 38) {
                        DropDownMenu(GUILayoutUtility.GetLastRect().max);
                    } else {
                        ClearLogs();
                        SaveLogs();
                    }
                }

                Handles.color = Color.black * 0.4f;
                Handles.DrawLine(_lastRect.position + new Vector2(38, 4), _lastRect.position + new Vector2(38, EditroMeasures.ToolbarHeight - 4));
                Handles.color = Color.white;

                GUILayout.Label(EditorGUIUtility.IconContent(_clearOnAwake ? "LockIcon" : "LockIcon-On"), EditorStyles.toolbarButton, GUILayout.Width(EditroMeasures.ToolbarHeight));
                #endregion

                GUILayout.FlexibleSpace();

                #region Filtering
                EditorGUILayout.BeginHorizontal();
                {
                    _filterMsgType[3] = GUILayout.Toggle(_filterMsgType[3], new GUIContent(_logsTypesCount[3].ToString(), EditorGUIUtility.IconContent("d_Progress").image), EditorStyles.toolbarButton);
                    _filterMsgType[0] = GUILayout.Toggle(_filterMsgType[0], new GUIContent(_logsTypesCount[0].ToString(), EditorGUIUtility.IconContent("console.infoicon.sml").image), EditorStyles.toolbarButton);
                    _filterMsgType[1] = GUILayout.Toggle(_filterMsgType[1], new GUIContent(_logsTypesCount[1].ToString(), EditorGUIUtility.IconContent("console.warnicon.sml").image), EditorStyles.toolbarButton);
                    _filterMsgType[2] = GUILayout.Toggle(_filterMsgType[2], new GUIContent(_logsTypesCount[2].ToString(), EditorGUIUtility.IconContent("console.erroricon.sml").image), EditorStyles.toolbarButton);
                }
                EditorGUILayout.EndHorizontal();

                GUILayout.Space(5);

                _filterObjectID = EditorGUILayout.IntPopup(_filterObjectID, _objNames, _objKeys, EditorStyles.toolbarPopup);
                #endregion
            }
            EditorGUILayout.EndHorizontal();
        }
        #endregion

        #region Upper Panel
        private void DrawUpperPanel (Event e) {
            GUILayout.BeginArea(new Rect(0, EditroMeasures.ToolbarHeight, position.width, _panelHeight));
            {
                if(e.type == EventType.MouseDown && e.mousePosition.y < _panelHeight - 10)
                    _selectedLogID = 0;

                _scroll = EditorGUILayout.BeginScrollView(_scroll, GUILayout.ExpandWidth(true));
                {
                    foreach(var log in _logs) {
                        if(_filterObjectID > 0 && log.Value.callerInstanceID != _logs[_filterObjectID].callerInstanceID)
                            continue;

                        switch(log.Value.type) {
                            case MessageType.None:
                                if (!_filterMsgType[3])
                                    continue;
                                break;
                            case MessageType.Info:
                                if(!_filterMsgType[0])
                                    continue;
                                break;
                            case MessageType.Warning:
                                if(!_filterMsgType[1])
                                    continue;
                                break;
                            case MessageType.Error:
                                if(!_filterMsgType[2])
                                    continue;
                                break;
                            default:
                                break;
                        }

                        GUI.color = _selectedLogID == log.Key ? EditorColors.ActiveBlue : EditorColors.NormalDark;

                        EditorGUILayout.BeginHorizontal(_skin.GetStyle("listElement"));
                        {
                            GUI.color = EditorColors.Base;

                            switch(log.Value.type) {
                                case MessageType.Info:
                                    EditorGUILayout.LabelField(EditorGUIUtility.IconContent("console.infoicon.sml"), GUILayout.Width(22));
                                    break;
                                case MessageType.Warning:
                                    EditorGUILayout.LabelField(EditorGUIUtility.IconContent("console.warnicon.sml"), GUILayout.Width(22));
                                    break;
                                case MessageType.Error:
                                    EditorGUILayout.LabelField(EditorGUIUtility.IconContent("console.erroricon.sml"), GUILayout.Width(22));
                                    break;
                                default:
                                    GUILayout.Space(28);
                                    break;
                            }

                            EditorGUILayout.LabelField(log.Value.title, EditorStyles.label, GUILayout.ExpandWidth(true));
                            EditorGUILayout.LabelField(log.Value.counter.ToString(), _skin.GetStyle("labelLeft"), GUILayout.Width(40));

                            GUILayout.Space(5);

                            if(GUILayout.Button(EditorGUIUtility.IconContent("UnityEditor.FindDependencies"), _skin.GetStyle("labelLeft"), GUILayout.Width(EditroMeasures.ToolbarHeight))) {
                                OpenFile(log.Value);
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        if(e.type == EventType.MouseDown && GUILayoutUtility.GetLastRect().Contains(e.mousePosition)) {
                            //if(_selectedLogID != log.Key)
                                _selectedLogID = log.Key;
                            //else if (_logs[_selectedLogID].callerInstanceID != 0)
                                //EditorGUIUtility.PingObject(EditorUtility.InstanceIDToObject(_logs[_selectedLogID].callerInstanceID));
                        }
                    }
                }
                EditorGUILayout.EndScrollView();
            }
            GUILayout.EndArea();
        }
        #endregion

        #region Lower Panel
        private void DrawLowerPanel (Event e) {
            if(_logs.Count <= 0 || !_logs.ContainsKey(_selectedLogID) || _logs[_selectedLogID] == null)
                return;

            GUILayout.BeginArea(new Rect(0, _panelHeight + EditroMeasures.ToolbarHeight, position.width, position.height - _panelHeight - EditroMeasures.ToolbarHeight));
            {
                var log = _logs[_selectedLogID];

                /// Log Title
                EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
                {
                    EditorGUILayout.LabelField(log.title, _skin.GetStyle("boldLabel"));
                }
                EditorGUILayout.EndHorizontal();

                /// Log Description
                if(!string.IsNullOrEmpty(log.content))
                    EditorGUILayout.LabelField(log.content, _skin.label, GUILayout.ExpandWidth(true));

                /// Log source file
                if(GUILayout.Button($"{log.file}", _skin.GetStyle("linkLabel"))) {
                    //EditorUtility.OpenWithDefaultApp(log.file);
                    OpenFile(log);
                }

                /// Log Reference Object (if it was given)
                if(log.callerInstanceID != 0) {
                    if(GUILayout.Button("Ping Source Object", _skin.button)) {
                        EditorGUIUtility.PingObject(EditorUtility.InstanceIDToObject(log.callerInstanceID));
                    }
                }
            }
            GUILayout.EndArea();
        }
        #endregion

        private void DropDownMenu (Vector2 sourcePos) {
            var menu = new GenericMenu();
            menu.AddItem(new GUIContent("Clear On Play"), _clearOnAwake, () => { _clearOnAwake = !_clearOnAwake; });
            menu.DropDown(new Rect(sourcePos + Vector2.up * EditroMeasures.ToolbarHeight, Vector2.zero));
        }
        #endregion

        #region Private Methods
        private void ClearLogs () {
            _logs.Clear();

            _objNames = new string[1] { "All" };
            _objKeys = new int[1] { -1 };

            _selectedLogID = 0;
            _filterObjectID = -1;

            _logsTypesCount[0] = 0;
            _logsTypesCount[1] = 0;
            _logsTypesCount[2] = 0;
            _logsTypesCount[3] = 0;
        }

        private void LoadLogs () {
            if(!File.Exists(_logsFile))
                return;

            //Debug.Log("Loading...");

            var jsonLogs = File.ReadAllText(_logsFile);
            var logs = JsonUtility.FromJson<Logs>(jsonLogs);

            foreach(var log in logs.logs) {
                Log(log);
            }
        }

        private void SaveLogs () {
            Logs logs = new Logs();

            foreach(var log in _logs) {
                logs.logs.Add(new LogInfo(log.Value));
            }

            var jsonLogs = JsonUtility.ToJson(logs, true);
            File.WriteAllText(_logsFile, jsonLogs);
        }

        private void OpenFile (LogInfo log) {
            foreach(var path in AssetDatabase.GetAllAssetPaths()) {
                if(path.EndsWith(Path.GetFileName(log.file)))
                    AssetDatabase.OpenAsset((MonoScript)AssetDatabase.LoadAssetAtPath<MonoScript>(path), log.line, 0);
            }
        }
        #endregion

        #region Public Functions
        public void Log (LogInfo logInfo) {
            if(!_logs.ContainsKey(logInfo.key)) {
                _logs.Add(logInfo.key, logInfo);

                if(logInfo.callerInstanceID != 0 && !_objNames.Contains(EditorUtility.InstanceIDToObject(logInfo.callerInstanceID).ToString())) {
                    _objNames = _objNames.Append($"{EditorUtility.InstanceIDToObject(logInfo.callerInstanceID)} ({EditorUtility.InstanceIDToObject(logInfo.callerInstanceID).GetType().Name})").ToArray();
                    _objKeys = _objKeys.Append(logInfo.key).ToArray();
                }

                switch(logInfo.type) {
                    case MessageType.Info:
                        _logsTypesCount[0]++;
                        break;
                    case MessageType.Warning:
                        _logsTypesCount[1]++;
                        break;
                    case MessageType.Error:
                        _logsTypesCount[2]++;
                        break;
                    default:
                        _logsTypesCount[3]++;
                        break;
                }
            } else {
                //_logs[logInfo.key].content = logInfo.title;
                _logs[logInfo.key].counter++;
            }

            Repaint();
        }
        #endregion
    }
}